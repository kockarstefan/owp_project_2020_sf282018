package com.sf282018.OWP.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.sf282018.OWP.model.Book;
import com.sf282018.OWP.model.User;
import com.sf282018.OWP.model.Book.BookCover;
import com.sf282018.OWP.service.BookService;



@Controller
@RequestMapping(value="/books")
public class BookController {

	@Autowired
	private BookService bookService;
	
	@Autowired
	private ServletContext servletContext;
	private String baseURL;
	
	@PostConstruct
	public void init() {
		baseURL = servletContext.getContextPath() + "/";
		
	}
	
	@GetMapping(value="/index") 
	public ModelAndView index(
			@RequestParam(required = false)	String searchBox,
			@RequestParam(required = false) String priceLow,
			@RequestParam(required = false) String priceHigh,
			HttpSession session) {
		ModelAndView result = new ModelAndView("index");
		List<Book> books = new ArrayList<Book>();
		if(searchBox == null || searchBox.trim().equals("")) {
			books = new ArrayList<Book>(bookService.findAll());
		}else {
			books = new ArrayList<Book>(bookService.search(searchBox, priceLow, priceHigh));
		}
			
		result.addObject("books", books);
		return result;
		}
			
//		List<Book> books = bookService.findAll(search);
//		ModelAndView result = new ModelAndView("index");
//		result.addObject("books", books);
//		return result;
//		}else {
//			List<Book> books = bookService.findAll();
//			ModelAndView result = new ModelAndView("index");
//			result.addObject("books", books);
//			return result;
//		}
		

	
	@GetMapping(value="/addBook")
	public ModelAndView addBook() {
		ModelAndView result = new ModelAndView("addBook");
		return result;
	}
	
	
	@GetMapping(value="/Details/{id}")
	public ModelAndView details(@PathVariable("id") int id,
			HttpSession session, HttpServletResponse response) throws IOException {
		// validacija
		Book book = bookService.findOne(id);
		if (book == null) {
			response.sendRedirect(baseURL + "books/index");
			return null;
		}

		// prosleđivanje
		ModelAndView result = new ModelAndView("book");
		result.addObject("book", book);

		return result;
	}
	
	@GetMapping(value="/edit/{id}")
	public ModelAndView edit(@PathVariable("id") int id,
			HttpSession session, HttpServletResponse response) throws IOException {
		// validacija
		Book book = bookService.findOne(id);
		if (book == null) {
			response.sendRedirect(baseURL + "books/index");
			return null;
		}

		// prosleđivanje
		ModelAndView result = new ModelAndView("editBook");
		result.addObject("book", book);

		return result;
	}
	
	@PostMapping(value="/addBook")
	public ModelAndView postAddBook(@RequestParam String name, 
			@RequestParam String ISBN, 
			@RequestParam String publisher, 
			@RequestParam String author, 
			@RequestParam String releaseDate, 
			@RequestParam String description, 
			@RequestParam double price, 
			@RequestParam int numOfPages, 
			@RequestParam String coverType, 
			@RequestParam String language, 
			@RequestParam String languageType, 
			@RequestParam String rating, HttpSession session, HttpServletResponse response) throws IOException{
	
		try {
			
			Book book = bookService.findOne(name);
			if(book!=null) {
				ModelAndView result = new ModelAndView("addBook");
				return result;
			} else {
				List<Book> books = bookService.findAll();
				int id = books.size() + 1;
				BookCover convertedCoverType = BookCover.valueOf(coverType);
				Book newBook = new Book(id, name, ISBN, publisher, author, releaseDate, description, price,numOfPages, convertedCoverType, language, languageType, rating);
				bookService.save(newBook);
				response.sendRedirect(baseURL + "books/index");
			}
			
		} catch (Exception ex) {
			ex.printStackTrace();
			}
		
		
		
		return null;
	}
	
	@PostMapping(value="edit/editBook")
	public ModelAndView postEditBook(
			@RequestParam int id,
			@RequestParam String name, 
			@RequestParam String ISBN, 
			@RequestParam String publisher, 
			@RequestParam String author, 
			@RequestParam String releaseDate, 
			@RequestParam String desc, 
			@RequestParam double price, 
			@RequestParam int numOfPages, 
			@RequestParam String coverType, 
			@RequestParam String language, 
			@RequestParam String languageType, 
			@RequestParam String rating, HttpSession session, HttpServletResponse response) throws IOException {
		
		try {
			//validation
			if (name.equals("")) {
				throw new Exception("Name field must not be empty!");
			}
			if (name.equals("") || publisher.equals("")) {
				throw new Exception("Name or publisher field must not be empty!");
			}
			if (ISBN.equals("") || author.equals("")) {
				throw new Exception("ISBN or author field must not be empty!");
			}
			if (desc.equals("")) {
				throw new Exception("Description field must not be empty!");
			}
			if (price<=0) {
				throw new Exception("Price must be higher than 0!");
			}
			if (numOfPages<=0) {
				throw new Exception("Page number must be higher than 0!");
			}
			if (coverType.equals("") || rating.equals("")) {
				throw new Exception("Cover type or rating field must not be empty!");
			}
			if (language.equals("") || languageType.equals("")) {
				throw new Exception("Language or language type field must not be empty!");
			}
			
			//editing
			Book book = bookService.findOne(id);
			book.setName(name);
			book.setISBN(ISBN);
			book.setPublisher(publisher);
			book.setAuthor(author);
			book.setReleaseDate(releaseDate);
			book.setDesc(desc);
			book.setPrice(price);
			book.setNumOfPages(numOfPages);
			book.setCoverType(BookCover.valueOf(coverType));
			book.setLanguage(language);
			book.setLanguageType(languageType);
			book.setRating(rating);
			bookService.update(book);
			response.sendRedirect(baseURL + "books/index");
			
		}catch(Exception ex) {
			ex.printStackTrace();
		}
		
		return null;
	}
	
	

}
