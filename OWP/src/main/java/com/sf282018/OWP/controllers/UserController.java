package com.sf282018.OWP.controllers;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.sf282018.OWP.model.User;
import com.sf282018.OWP.model.User.Role;
import com.sf282018.OWP.service.UserService;
import com.sf282018.OWP.util.DateTimeUtil;

@Controller
@RequestMapping(value="/Users")
public class UserController {
	
	public static final String USER_KEY = "loggedInUser";
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private ServletContext servletContext;
	private String baseURL;
	
	@PostConstruct
	public void init() {
		baseURL = servletContext.getContextPath() + "/";
		
	}
	
	@GetMapping(value="/Login")
	public ModelAndView login() {
		ModelAndView result = new ModelAndView("login");
		return result;
	}
	
	@GetMapping(value="/Register")
	public ModelAndView register() {
		ModelAndView result = new ModelAndView("registration");
		return result;
	}
	
	
	@PostMapping(value="Users/Login")
	public ModelAndView postLogin(@RequestParam String username, @RequestParam String password, 
			HttpSession session, HttpServletResponse response) throws IOException {
		try {
			System.out.println(username);
			System.out.println(password);
			// validation
			User user = userService.findOne(username, password);
			if (user == null) {
				throw new Exception("Wrong username or password!");
			}			

			// login
			session.setAttribute(UserController.USER_KEY, user);
			
			response.sendRedirect(baseURL + "books/index");
			return null;
		} catch (Exception ex) {
			// message output
			String message = ex.getMessage();
			if (message == "") {
				message = "Login unsuccessful!";
			}
			
			// forwarding
			ModelAndView result = new ModelAndView("login");
			result.addObject("message", message);

			return result;
		}
	}
	
	@PostMapping(value="Users/Register")
	public ModelAndView register(@RequestParam String username, 
			@RequestParam String password, 
			@RequestParam String repeatedPassword,
			@RequestParam String email,
			@RequestParam String name,
			@RequestParam String surname,
			@RequestParam String dateOfBirth,
			@RequestParam String address,
			@RequestParam String phoneNum, HttpSession session, HttpServletResponse response)throws IOException {
		
		try {
			// validation
						User existingUser = userService.findOne(username);
						if (existingUser != null) {
							throw new Exception("Username already exists!");
						}
						if (username.equals("") || password.equals("")) {
							throw new Exception("Username or password field must not be empty!");
						}
						if (!password.equals(repeatedPassword)) {
							throw new Exception("Password and repeated password don't match!");
						}
						if (email.equals("")) {
							throw new Exception("Email field must not be empty!");
						}
						if (name.equals("") || surname.equals("")) {
							throw new Exception("Name or surname field must not be empty!");
						}
						if (dateOfBirth.equals("")) {
							throw new Exception("Date of birth field must not be empty!");
						}
						if (address.equals("")) {
							throw new Exception("Address field must not be empty!");
						}
						if (phoneNum.equals("")) {
							throw new Exception("Phone number field must not be empty!");
						}
			//registration
				List<User> users = userService.findAll();
				int id = users.size() + 1;
				
				LocalDateTime birthDate = DateTimeUtil.StringToLocalDateTime(dateOfBirth);
				
				User newUser = new User(id, username, password, email, name, surname, birthDate,address, phoneNum, LocalDateTime.now(), Role.CUSTOMER);
				userService.save(newUser);
				response.sendRedirect(baseURL + "Users/Login");
				ModelAndView result = new ModelAndView("login");
				return result;
				
		} catch (Exception ex) {
			// message output
			String message = ex.getMessage();
			if (message == "") {
				message = "Registration unsuccessful!";
			}
			
			// forwarding
			ModelAndView result = new ModelAndView("registration");
			result.addObject("message", message);

			return result;
		}
		
	}
	
}
