package com.sf282018.OWP.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sf282018.OWP.dao.BookDAO;
import com.sf282018.OWP.model.Book;
import com.sf282018.OWP.service.BookService;

@Service
public class DatabaseBookService implements BookService{

	
	@Autowired
	private BookDAO bookDAO;
	
	@Override
	public Book findOne(int id) {
		return bookDAO.findOne(id);
	}

	@Override
	public Book findOne(String name) {
		return bookDAO.findOne(name);
	}

	

	@Override
	public List<Book> findAll() {
		return bookDAO.findAll();
	}
	
	@Override
	public Book save(Book book) {
		bookDAO.save(book);
		return book;
	}

	@Override
	public Book update(Book book) {
		bookDAO.update(book);
		return book;
	}

	@Override
	public Book delete(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Book> search(String searchBox, String priceLow, String priceHigh) {
		return bookDAO.search(searchBox, priceLow, priceHigh);
	}

	
	
}
