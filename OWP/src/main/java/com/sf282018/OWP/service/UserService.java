package com.sf282018.OWP.service;

import java.util.List;

import com.sf282018.OWP.model.User;

public interface UserService {

	User findOne(String username);
	User findOne(String username, String password);
	List<User> findAll();
	User save(User user);
	User update(User user);
	User delete(String username);

}
