package com.sf282018.OWP.service;

import java.util.List;

import com.sf282018.OWP.model.Book;


public interface BookService {

	Book findOne(int id);
	Book findOne(String name);
	List<Book> search(String searchBox,String priceLow,String priceHigh);
	List<Book> findAll();
	Book save(Book book);
	Book update(Book book);
	Book delete(int id);

}
