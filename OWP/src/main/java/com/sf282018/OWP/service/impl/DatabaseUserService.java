package com.sf282018.OWP.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sf282018.OWP.dao.UserDAO;
import com.sf282018.OWP.model.User;
import com.sf282018.OWP.service.UserService;

@Service
public class DatabaseUserService implements UserService {
	
	@Autowired
	private UserDAO userDAO;
	
	@Override
	public User findOne(String username) {
		return userDAO.findOne(username);
	}
	
	@Override
	public User findOne(String username, String password) {
		return userDAO.findOne(username, password);
	}
	
	@Override
	public List<User> findAll() {
		return userDAO.findAll();
	}
	
	@Override
	public User save(User user) {
		userDAO.save(user);
		return user;
	}

	@Override
	public User update(User user) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User delete(String username) {
		// TODO Auto-generated method stub
		return null;
	}
	
	
}
