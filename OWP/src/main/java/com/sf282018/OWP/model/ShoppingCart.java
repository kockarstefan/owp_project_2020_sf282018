package com.sf282018.OWP.model;

import java.time.LocalDateTime;
import java.util.List;

public class ShoppingCart {

	private List<Book> books;
	private double price;
	private LocalDateTime date;
	private User user;
	private int bookCount;
	
	public ShoppingCart() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ShoppingCart(List<Book> books, double price, LocalDateTime date, User user, int bookCount) {
		super();
		this.books = books;
		this.price = price;
		this.date = date;
		this.user = user;
		this.bookCount = bookCount;
	}

	public List<Book> getBooks() {
		return books;
	}

	public void setBooks(List<Book> books) {
		this.books = books;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public LocalDateTime getDate() {
		return date;
	}

	public void setDate(LocalDateTime date) {
		this.date = date;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public int getBookCount() {
		return bookCount;
	}

	public void setBookCount(int bookCount) {
		this.bookCount = bookCount;
	}
	
	
}
