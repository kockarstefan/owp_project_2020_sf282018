package com.sf282018.OWP.model;

import java.time.LocalDateTime;

public class Comment {
 
	private String text;
	private int rating;
	private LocalDateTime postDate;
	private User user;
	private Book book;
	private boolean status;
	
	public Comment() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Comment(String text, int rating, LocalDateTime postDate, User user, Book book, boolean status) {
		super();
		this.text = text;
		this.rating = rating;
		this.postDate = postDate;
		this.user = user;
		this.book = book;
		this.status = status;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public int getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}

	public LocalDateTime getPostDate() {
		return postDate;
	}

	public void setPostDate(LocalDateTime postDate) {
		this.postDate = postDate;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Book getBook() {
		return book;
	}

	public void setBook(Book book) {
		this.book = book;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}
	
	
	
}
