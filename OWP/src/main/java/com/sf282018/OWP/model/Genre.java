package com.sf282018.OWP.model;

public class Genre {
		
	private String type;
	private String desc;
	
	
	public Genre() {
		super();
	}


	public Genre(String type, String desc) {
		super();
		this.type = type;
		this.desc = desc;
	}


	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}


	public String getDesc() {
		return desc;
	}


	public void setDesc(String desc) {
		this.desc = desc;
	}
	
	
	
}
