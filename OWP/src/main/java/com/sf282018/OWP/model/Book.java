package com.sf282018.OWP.model;

public class Book {

	private int id;
	private String name;
	private String ISBN;
	private String publisher;
	private String author;
	private String releaseDate;
	private String desc;
	private double price;
	private int numOfPages;
	private BookCover coverType;
	private String language;
	private String languageType;
	private String rating;
	
	public enum BookCover { hardcover, softcover}

	public Book() {
		super();
	}

	public Book(int id, String name, String ISBN, String publisher, String author, String releaseDate, String desc,
			double price, int numOfPages, BookCover coverType, String language, String languageType, String rating) {
		super();
		this.id = id;
		this.name = name;
		this.ISBN = ISBN;
		this.publisher = publisher;
		this.author = author;
		this.releaseDate = releaseDate;
		this.desc = desc;
		this.price = price;
		this.numOfPages = numOfPages;
		this.coverType = coverType;
		this.language = language;
		this.languageType = languageType;
		this.rating = rating;
	}

	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getISBN() {
		return ISBN;
	}

	public void setISBN(String iSBN) {
		ISBN = iSBN;
	}

	public String getPublisher() {
		return publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(String releaseDate) {
		this.releaseDate = releaseDate;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getNumOfPages() {
		return numOfPages;
	}

	public void setNumOfPages(int numOfPages) {
		this.numOfPages = numOfPages;
	}

	public BookCover getCoverType() {
		return coverType;
	}

	public void setCoverType(BookCover coverType) {
		this.coverType = coverType;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getLanguageType() {
		return languageType;
	}

	public void setLanguageType(String languageType) {
		this.languageType = languageType;
	}

	public String getRating() {
		return rating;
	}

	public void setRating(String rating) {
		this.rating = rating;
	}
	
	
	
	
	
}
