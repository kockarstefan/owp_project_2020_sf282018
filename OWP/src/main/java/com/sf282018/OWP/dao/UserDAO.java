package com.sf282018.OWP.dao;

import java.util.List;

import com.sf282018.OWP.model.User;

public interface UserDAO {
	
	public User findOne(String username);

	public User findOne(String username, String password);

	public List<User> findAll();
	
	public void save(User user);

	public void update(User user);

	public void delete(String username);
}
