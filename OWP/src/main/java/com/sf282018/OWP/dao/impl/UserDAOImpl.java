package com.sf282018.OWP.dao.impl;



import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.sf282018.OWP.dao.UserDAO;
import com.sf282018.OWP.model.User;
import com.sf282018.OWP.model.User.Role;
import com.sf282018.OWP.util.DateTimeUtil;



@Repository
public class UserDAOImpl implements UserDAO{
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	private class UserRowMapper implements RowMapper<User> {
		
		@Override
		public User mapRow(ResultSet rs, int rowNum) throws SQLException {
			int index = 1;
			int id = rs.getInt(index++);
			String username = rs.getString(index++);
			String password = rs.getString(index++);
			String email = rs.getString(index++);
			String name = rs.getString(index++);
			String surname = rs.getString(index++);
			String timeStamp = rs.getString(index++);
			LocalDateTime dateOfBirth = DateTimeUtil.StringToLocalDateTime(timeStamp);
			String address = rs.getString(index++);
			String phoneNum = rs.getString(index++);
			String timeStamp2 = rs.getString(index++);
			LocalDateTime registrationDate = DateTimeUtil.StringToLocalDateTime(timeStamp2);
			Role role = Role.valueOf(rs.getString(index++));
			

			User user = new User(id,username, password, email, name, surname, dateOfBirth, address, phoneNum, registrationDate, role);
			return user;
		}
	}
	
	@Override
	public User findOne(String username) {
		try {
			String sql = "SELECT * FROM users WHERE username = ?";
			return jdbcTemplate.queryForObject(sql, new UserRowMapper(), username);
		} catch (EmptyResultDataAccessException ex) {
			//user not found
			return null;
		}
	}
	
	@Override
	public User findOne(String username, String password) {
		try {
			String sql = "SELECT * FROM users WHERE username = ? AND password = ?";
			return jdbcTemplate.queryForObject(sql, new UserRowMapper(), username, password);
		} catch (EmptyResultDataAccessException ex) {
			// user not found
			return null;
		}
	}
	
	@Override
	public List<User> findAll() {
		String sql = "SELECT * FROM users";
		return jdbcTemplate.query(sql, new UserRowMapper());
	}

	@Override
	public void save(User user) {
		String sql = "INSERT INTO users (id, username, password, email, name, surname, dateOfBirth, adress, phoneNum, regDate, Role) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		jdbcTemplate.update(sql,user.getId(), user.getUsername(), user.getPassword(), user.getEmail(), user.getName(), user.getSurname(), user.getDateOfBirth(), user.getAddress(), user.getPhoneNum(), user.getRegDate(), user.getRole().toString());

	}

	@Override
	public void update(User user) {
		System.out.println("Yet to be implemented...");
		
	}

	@Override
	public void delete(String username) {
		System.out.println("Yet to be implemented...");
		
	}
	
	
	

}
