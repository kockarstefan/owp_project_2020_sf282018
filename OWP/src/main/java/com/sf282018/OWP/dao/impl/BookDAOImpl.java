package com.sf282018.OWP.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.sf282018.OWP.dao.BookDAO;
import com.sf282018.OWP.model.Book;
import com.sf282018.OWP.model.User;
import com.sf282018.OWP.model.Book.BookCover;
import com.sf282018.OWP.model.User.Role;
import com.sf282018.OWP.util.DateTimeUtil;

@Repository
public class BookDAOImpl implements BookDAO {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	private class BookRowMapper implements RowMapper<Book> {
		
		@Override
		public Book mapRow(ResultSet rs, int rowNum) throws SQLException {
			int index = 1;
			int id = rs.getInt(index++);
			String name = rs.getString(index++);
			String isbn = rs.getString(index++);
			String publisher = rs.getString(index++);
			String author = rs.getString(index++);
			String releaseDate = rs.getString(index++);
			String description = rs.getString(index++);
			double price = rs.getDouble(index++);
			int numOfPages = rs.getInt(index++);
			BookCover covertype = BookCover.valueOf(rs.getString(index++));
			String language = rs.getString(index++);
			String languageType = rs.getString(index++);
			String rating = rs.getString(index++);
			

			Book book = new Book(id, name, isbn, publisher, author, releaseDate, description, price, numOfPages, covertype, language, languageType, rating);
			return book;
		}
	}
	
	
	@Override
	public Book findOne(int id) {
		try {
			String sql = "SELECT * FROM books WHERE id = ?";
			return jdbcTemplate.queryForObject(sql, new BookRowMapper(), id);
		} catch (EmptyResultDataAccessException ex) {
			//book not found
			return null;
		}
	}

	@Override
	public Book findOne(String name) {
		try {
			String sql = "SELECT * FROM books WHERE name = ?";
			return jdbcTemplate.queryForObject(sql, new BookRowMapper(), name);
		} catch (EmptyResultDataAccessException ex) {
			//book not found
			return null;
		}
	}

	
	@Override
	public List<Book> findAll() {
		String sql = "SELECT * from books";
		return jdbcTemplate.query(sql, new BookRowMapper());
	}

	@Override
	public void save(Book book) {
		String sql = "INSERT INTO books (id, name, ISBN, publisher,author, releaseDate, description, price, numOfPages, coverType, language, languageType, rating) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		jdbcTemplate.update(sql,book.getId(), book.getName(), book.getISBN(), book.getPublisher(), book.getAuthor(), book.getReleaseDate(), book.getDesc(), book.getPrice(), book.getNumOfPages(), book.getCoverType().toString(), book.getLanguage(), book.getLanguageType(), book.getRating());
	}

	@Override
	public void update(Book book) {
		String sql = "UPDATE books SET name = ?, ISBN = ?, publisher = ?, author = ?, releaseDate = ?, description = ?, price = ?, numOfPages = ?, coverType = ?, language = ?, languageType = ?, rating = ? WHERE id = ?";
		jdbcTemplate.update(sql, book.getName(), book.getISBN(), book.getPublisher(), book.getAuthor(), book.getReleaseDate(), book.getDesc(), book.getPrice(), book.getNumOfPages(), book.getCoverType().toString(), book.getLanguage(), book.getLanguageType(), book.getRating(), book.getId());
		
	}

	@Override
	public void delete(int id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Book> search(String searchBox, String priceLow, String priceHigh) {
		
		StringBuilder sql = new StringBuilder();
		if(searchBox != null) {
		sql.append("SELECT * from books WHERE name like '%" + searchBox + "%' OR author LIKE '%"+ searchBox + "%'");
		}
		if(priceLow != null) {
			try {
				 int priceLowInt = Integer.parseInt(priceLow);
				 sql.append(" AND price >= " + priceLowInt + " ");
			}catch(Exception ex) {
				ex.printStackTrace();
			}
			
		}
		
		if(priceHigh != null) {
			try {
				 int priceHighInt = Integer.parseInt(priceHigh);
				 sql.append(" AND price <= " + priceHighInt + " ");
			}catch(Exception ex) {
				ex.printStackTrace();
			}
		}

		
		return jdbcTemplate.query(sql.toString(), new BookRowMapper());
			
	}

}
