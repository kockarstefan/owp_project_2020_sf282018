package com.sf282018.OWP.dao;

import java.util.List;

import com.sf282018.OWP.model.Book;
import com.sf282018.OWP.model.User;

public interface BookDAO {

	
	public Book findOne(int id);

	public Book findOne(String name);

	public List<Book> search(String searchBox,String priceLow,String priceHigh);
	
	public List<Book> findAll();
	
	public void save(Book book);

	public void update(Book book);

	public void delete(int id);
}
