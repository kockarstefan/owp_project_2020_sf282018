package com.sf282018.OWP.listeners;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.springframework.stereotype.Component;

@Component
public class InitServletContextListener implements ServletContextListener {
	
	public void contextInitialized(ServletContextEvent event)  {
    	System.out.println("Updating context...");
    	
    	System.out.println("Context updated successfully!");
    }
    
    
	public void contextDestroyed(ServletContextEvent event)  { 
    	System.out.println("Deleting context...");
    		
    	System.out.println("Context deleted successfully!");
    }
}
