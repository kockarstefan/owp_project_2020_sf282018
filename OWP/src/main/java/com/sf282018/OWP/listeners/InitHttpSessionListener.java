package com.sf282018.OWP.listeners;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.springframework.stereotype.Component;

@Component
public class InitHttpSessionListener implements HttpSessionListener {
	
	
	public void sessionCreated(HttpSessionEvent event) {
		System.out.println("Session initializing..");

		
		HttpSession session  = event.getSession();
		System.out.println("User session id is: "+ session.getId());


		System.out.println("Session initialized successfully!");
	}
	

	public void sessionDestroyed(HttpSessionEvent arg0) {
		System.out.println("Deleting session...");
		
		System.out.println("Successfully deleted session!");
	}
}
