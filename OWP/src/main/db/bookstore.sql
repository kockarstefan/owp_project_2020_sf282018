CREATE TABLE `bookstore`.`users` (
  `id` INT NOT NULL,
  `username` VARCHAR(20) NOT NULL,
  `password` VARCHAR(20) NOT NULL,
  `email` VARCHAR(45) NOT NULL,
  `name` VARCHAR(25) NOT NULL,
  `surname` VARCHAR(30) NOT NULL,
  `dateOfBirth` DATETIME NOT NULL,
  `adress` VARCHAR(45) NOT NULL,
  `phoneNum` VARCHAR(15) NOT NULL,
  `regDate` DATETIME NOT NULL,
  `Role` VARCHAR(10) NOT NULL,
  PRIMARY KEY (`id`));

INSERT INTO users (id, username, password, email, name, surname, dateOfBirth, adress, phoneNum, regDate, Role) VALUES (1, 'petarp', 'petar123', 'petarp@live.com', 'Petar', 'Petrovic', '1990-01-01 01:01:00','Mainstreet', '1122334455', now(), 'ADMIN')

CREATE TABLE `bookstore`.`books` (
  `id` INT NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  `ISBN` VARCHAR(13) NOT NULL,
  `publisher` VARCHAR(30) NOT NULL,
  `author` VARCHAR(25) NOT NULL,
  `releaseDate` VARCHAR(10) NOT NULL,
  `description` VARCHAR(150) NOT NULL,
  `price` DOUBLE NOT NULL,
  `numOfPages` INT NOT NULL,
  `coverType` VARCHAR(10) NOT NULL,
  `language` VARCHAR(25) NOT NULL,
  `languageType` VARCHAR(10) NOT NULL,
  `rating` VARCHAR(10) NOT NULL,
  PRIMARY KEY (`id`));

INSERT INTO books (id, name, ISBN, publisher,author, releaseDate, description, price, numOfPages, coverType, language, languageType, rating) values (1, 'The Hunger Games','9780439023528', 'Scholastic Press', 'Suzanne Collins', '2008', 'Written in the voice of 16-year-old Katniss Everdeen, who lives in the future, post-apocalyptic nation in North America.', '1000', '374','hardcover', 'English', 'latin', '7.2/10')